﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PetHotel.Models
{
    public class PetDbContext : DbContext
    {
        public PetDbContext() : base("name=PetDBContext")
        {

        }

        public DbSet<Pet> Pets { get; set; }
    }
}