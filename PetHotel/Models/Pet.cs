﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PetHotel.Models
{
    public class Pet
    {
        public int Id { get; set; }
        public String Specie { get; set; }
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }
    }
}