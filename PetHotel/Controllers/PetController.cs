﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PetHotel.Models;

namespace PetHotel.Controllers
{
    public class PetController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            using (var petDb = new PetDbContext())
            {
                var pets = petDb.Pets.ToList();
                return View(pets);
            }
        }

    }
}